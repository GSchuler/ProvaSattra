Prova Sattra - 16/02/2018

Ser� avaliado: 
1. Clareza do c�digo
2. Cobertura de testes
3. Padr�es de projetos
4. Documenta��o produzida


Notas do desenvolvedor:

1. A linguagem utilizada foi C#, porque com ela posso desempenhar o mesmo papel que o java poderia, mas usando ela eu conseguiria expor o meu potencial m�ximo);
2. Banco de dados utilizado: MariaDB;
3. Ferramenta usada para fazer testes: NUnit;
4. Testes cobertos:
  - 1. Verificar se os campos inteiros e chars correspondem;
  - 2. Verficar se o campo n�o ultrapassa o limite no bd;
  - 3. N�o permitir preencher  os campos de logradouro, bairro, localidade e uf;
  - 4. Verificar se os campos not null foram preenchidos
  - 5. Validar o CPF
5. Funcionalidades:
  - 1. O sistema � capaz de inserir registros na tabela Pessoas;
  - 2. O sistema � capaz de alterar registros inseridos na tabela Pessoas;
  - 3. O sistema � capaz de deletar registros inseridos na tabela Pessoas;
  - 4. O sistema � capaz de listar registros da tabela Pessoas;
  - 5. O sistema � capaz de exportar registros uma vez listados para um arquivo texto;
  - 6. O sistema � capaz de verificar se um CPF � v�lido ou n�o;
  - 7. O sistema � capaz de autopreencher campos predefinidos (como bairro, logradouro, uf e localidade), uma vez que o CEP � inserido.
6. Padr�es de desenvolvimento utilizados:
  - 1. O sistema foi feito sob uma camada de interface com o usu�rio (ProvaSattra) que chama uma classe de comando (CommandControl) para executar a��es no banco de dados:
	 a. Na classe Form1.cs � onde se localiza a interface com o usu�rio, nela podemos navegar e dar manuten��o em qualquer elemento visual;
	 b. A classe CommandControl foi criada para estabelecer uma liga��o (interface) com a camada DAO e a camada VISUAL, portanto, nela temos os m�todos mais simplificados e n�o-diretamente acessados na camada DAO.
  - 2. No sistema � poss�vel observar um projeto (ProvaSattra.DAO) espec�fico para se comunicar com o banco de dados, nele temos:
     a. A classe SattraDAO, que � respons�vel por executar as a��es (scripts) no banco de dados;
	 b. A classe ConnectionClass, que � respons�vel por manter a conex�o (leia-se: "conectar, desconectar e atestar atual conex�o") com o banco de dados.
  - 3. No sistema � poss�vel observer um projeto (ProvaSattra.BO) espec�fico para o padr�o de Classe/Objeto que ser� manuseado no c�digo:
	 a. A classe PessoaBO � respons�vel por armazenar todo dano relativo a tabela Pessoas no banco de dados, que ser� manuseado no c�digo;
	 b. A classe PessoaTableFieldBO � respons�vel por armazenar os nomes dos campos que est�o presentes no banco de dados ("Nome", "Email", "CPF", ...);
	 c. A mesma classe PessoaTableFieldBO tamb�m foi programada para pegar os nomes desses campos pelo arquivo de configura��o (ProvaSattra.App.Config<AppSettings>), caso seja necess�ria a mudan�a do nome dos campos sem a altera��o do c�digo;
	 d. A classe CepBO foi feita para espelhar o JSON que � retornado do site da VIACEP, com isso podemos serializar de forma simples todo json retornado do site e usar da forma desejada no c�digo;
	 e. A classe ValidateFields � uma classe est�tica que pode ser acionada sempre que a checagem de um campo � necess�ria. Nela temos as regras de como um campo deve se comportar para que n�o seja violado no banco e durante o preenchimento;
  - 4. No sistema � poss�vel observar o projeto ProvaSattra.ExternalFunctions, nele temos a �nica classe "ViaCEP", que � a classe respons�vel por se conectar ao "mundo externo" e capturar informa��es que ser�o convertidas para o padr�o usado no c�digo;
  - 5. Por fim, temos o projeto de testes, que � simplesmente utilizado para conferir se m�todos previamente constru�dos se comportam como o esperado na hora de uma chamada isolada.