/* Criando DB */
CREATE DATABASE 'Prova';

USE 'Prova';

/* Criando tabela */

create table Pessoas(
id bigint AUTO_INCREMENT PRIMARY KEY,
nome varchar(255) not null,
cpf varchar(11) not null, /*Precisa ser validado no c�digo*/
telefone varchar(14) not null, /*padr�o considerado: '+5511999212270'*/
email varchar(255) not null,
cep varchar(8) not null,
logradouro varchar(255) not null,
bairro varchar(255) not null,
localidade varchar(255) not null,
uf varchar(2) not null,
numero varchar(255) not null,
complemento varchar(255) null,
observacoes varchar(255) null
);
select * from Pessoas;

/* Inser��o do Primeiro Registro */

insert into Pessoas (nome, cpf, telefone, email, cep, logradouro, bairro, localidade, uf, numero, complemento, observacoes)
values ('Gianlucca Schuler', '12345678901', '+5511999212270', 'gianlucca.schuler@gmail.com', '24220390', 'Travessa Dom Bosco', 'Icara�', 'Niter�i', 'RJ', '13', '', ''); 
select * from Pessoas;

/* Editar campos */

update Pessoas
set nome='Gianlucca Schuler F. S.'
where cpf='12345678901';
select * from Pessoas;


/* Deletando o Primeiro registro */

delete from Pessoas where cpf='12345678901';
select * from Pessoas;