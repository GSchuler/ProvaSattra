Drop table Pessoas;

create table Pessoas(
id bigint AUTO_INCREMENT PRIMARY KEY,
nome varchar(255) not null,
cpf varchar(11) not null, /*Precisa ser validado no código*/
telefone varchar(11) not null, /*padrão considerado: '11999212270'*/
email varchar(255) not null,
cep varchar(8) not null,
logradouro varchar(255) not null,
bairro varchar(255) not null,
localidade varchar(255) not null,
uf varchar(2) not null,
numero varchar(255) not null,
complemento varchar(255) null,
observacoes varchar(255) null
);

insert into Pessoas (nome, cpf, telefone, email, cep, logradouro, bairro, localidade, uf, numero, complemento, observacoes)
values ('Gianlucca Schuler', '12345678901', '11999212270', 'gianlucca.schuler@gmail.com', '24220390', 'Travessa Dom Bosco', 'Icaraí', 'Niterói', 'RJ', '13', '', ''); 

insert into Pessoas (nome, cpf, telefone, email, cep, logradouro, bairro, localidade, uf, numero, complemento, observacoes)
values ('Anne Schuler', '22222222222', '21999010200', 'anne.schuler@gmail.com', '12312333', 'Rua Miguel de Frias', 'Centro', 'Niterói', 'RJ', '12', '', '');


select * from Pessoas;

select * from Pessoas where bairro like '%ica%';

update Pessoas
set nome='Gianlucca Schuler F. S.'
where cpf='12345678901';

select * from Pessoas;



/*delete from Pessoas where cpf='12345678901';*/

select * from Pessoas;

