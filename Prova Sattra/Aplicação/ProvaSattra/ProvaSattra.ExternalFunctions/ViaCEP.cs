﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using ProvaSattra.BO;

namespace ProvaSattra.ExternalFunctions
{
    public class ViaCEP
    {
        public CepBo GetDataByCep(string cep)
        {
            var cbo = new CepBo();
            try
            {


                var url = "http://viacep.com.br/ws/" + cep + "/json";

                var request = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)request.GetResponse();

                var stream = response.GetResponseStream();
                var reader = new StreamReader(stream);

                JavaScriptSerializer js = new JavaScriptSerializer();

                cbo = (CepBo)js.Deserialize(reader.ReadToEnd(), typeof(CepBo));
            }
            catch
            {
                return cbo;
            }

            return cbo;
        }
        
    }
}
