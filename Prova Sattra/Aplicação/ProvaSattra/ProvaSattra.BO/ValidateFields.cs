﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProvaSattra.BO
{
    public static class ValidateFields
    {

        public static string ValidateNome(this string nome)
        {
            var ttText = "";
            var count = 0;

            if (nome.Length > 255)
            {
                count++;
                ttText += "O tamanho do texto excede o limite (255)";
            }

            if (String.IsNullOrEmpty(nome.Trim()))
            {
                count++;
                ttText += "O campo não pode estar vazio";
            }

            if (count == 0)
                ttText += "Parece tudo ok";

            return ttText;
        }

        public static string ValidateCpf(this string cpf)
        {
            var ttText = "";
            var count = 0;

            var multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            var multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            string tempCpf;

            string digito;

            int soma;

            int resto;

            cpf = cpf.Trim();

            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
            {
                count++;
                ttText = "CPF Inválido";
                return ttText;
            }

            tempCpf = cpf.Substring(0, 9);

            soma = 0;

            for (int i = 0; i < 9; i++)

                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;

            if (resto < 2)

                resto = 0;

            else

                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;

            for (int i = 0; i < 10; i++)

                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;

            if (resto < 2)

                resto = 0;

            else

                resto = 11 - resto;

            digito = digito + resto.ToString();

            if (!cpf.EndsWith(digito))
            {
                count++;
                ttText = "CPF Inválido";
                return ttText;
            }

            if (count == 0)
                ttText += "Parece tudo ok";

            return ttText;
        }

        public static string ValidateEmail(this string email)
        {
            var indexArr = email.Trim().IndexOf('@');
            if (indexArr > 0 && email.Length < 255)
            {
                var indexDot = email.IndexOf('.', indexArr);
                if (indexDot - 1 > indexArr)
                {
                    if (indexDot + 1 < email.Length)
                    {
                        var indexDot2 = email.Substring(indexDot + 1, 1);
                        if (indexDot2 != ".")
                        {
                            return "Parece tudo ok";
                        }
                    }
                }
            }
            return "Email inválido";
        }

        public static string ValidateTelefone(this string tel)
        {
            var txt = "Número de telefone inválido";
            try
            {
                if (tel.Trim().Length > 0 && tel.Trim().Length <= 11)
                {
                    Convert.ToInt64(tel.Trim());
                    return "Parece tudo ok";
                }
                return txt;
            }
            catch
            {
                return txt;
            }

        }
    }
}
