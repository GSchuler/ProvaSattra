﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvaSattra.BO
{
    public class PessoaTableFieldsBO
    {
        public string TablePessoa { get; private set; }
        public string Id { get; set; }
        public string Nome { get; private set; }
        public string Cpf { get; private set; }
        public string Telefone { get; private set; }
        public string Email { get; private set; }
        public string Cep { get; private set; }
        public string Logradouro { get; private set; }
        public string Bairro { get; private set; }
        public string Localidade { get; private set; }
        public string Uf { get; private set; }
        public string Numero { get; private set; }
        public string Complemento { get; private set; }
        public string Observacoes { get; private set; }

        public PessoaTableFieldsBO()
        {
            var config = System.Configuration.ConfigurationManager.AppSettings;
            TablePessoa = config["TablePessoa"];
            Id = config["Id"];
            Nome = config["Nome"];
            Cpf = config["Cpf"];
            Telefone = config["Telefone"];
            Email = config["Email"];
            Cep = config["Cep"];
            Logradouro = config["Logradouro"];
            Bairro = config["Bairro"];
            Localidade = config["Localidade"];
            Uf = config["Uf"];
            Numero = config["Numero"];
            Complemento = config["Complemento"];
            Observacoes = config["Observacoes"];
        }
    }
}
