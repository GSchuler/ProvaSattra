﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvaSattra.BO
{
    public class PessoaBO
    {
        public string TableName { get; private set; }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Observacoes { get; set; }

        public PessoaBO()
        {
            TableName = System.Configuration.ConfigurationManager.AppSettings["TablePessoa"];
            Complemento = "";
            Observacoes = "";
        }
    }
}
