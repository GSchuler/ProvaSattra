﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProvaSattra.BO;
using ProvaSattra.DAO;
using ProvaSattra.ExternalFunctions;

namespace ProvaSattra.Test
{
    /*
     - Não foi identificada a necessidade de construir métodos com "dryRun" dentro do código oficial, 
     portanto os métodos que se comunicam com banco de dados não foram explorados nos testes.
    */
    [TestClass]
    public class UnitTestSattra
    {
        private string sucess = "Parece tudo ok";

        [TestMethod]
        public void TestCPF()
        {
            var cpf = "31555193803";
            Assert.IsTrue(cpf.ValidateCpf() == sucess);

            cpf = "12312321";
            Assert.IsFalse(cpf.ValidateCpf() == sucess);
        }

        [TestMethod]
        public void TestNome()
        {
            var nome = "Gianlucca Schuler";
            Assert.IsTrue(nome.ValidateNome() == sucess);

            nome = "";
            Assert.IsFalse(nome.ValidateNome() == sucess);
        }

        [TestMethod]
        public void TestCEP()
        {
            var viacep = new ViaCEP();
            var cep = new CepBo();
            cep = viacep.GetDataByCep("01549020");

            Assert.IsNotNull(cep);

            cep = viacep.GetDataByCep("1231231222");

            Assert.IsNull(cep.Logradouro);
        }

        [TestMethod]
        public void TestTelefone()
        {
            var tel = "11999212270";
            Assert.IsTrue(tel.ValidateTelefone() == sucess);

            tel = "119992122312312370";
            Assert.IsFalse(tel.ValidateTelefone() == sucess);
        }

        [TestMethod]
        public void TestEmail()
        {
            var email = "gianlucca.schuler@gmail.com";
            Assert.IsTrue(email.ValidateEmail() == sucess);

            email = "gianlucca.schuler";
            Assert.IsFalse(email.ValidateEmail() == sucess);
        }
    }
}
