﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using ProvaSattra.BO;
using System.Data;
using ProvaSattra.DAO;

namespace ProvaSattra.DAO
{
    public class SattraDAO
    {
        private ConnectionClass cc;
        public SattraDAO()
        {
            cc = new ConnectionClass();
        }


        public bool Insert(PessoaBO p)
        {
            try
            {
                if (cc.OpenConnection() == true)
                {
                    var ptf = new PessoaTableFieldsBO();
                    var fieldNames = ptf.Nome + ", " +
                                     ptf.Cpf + ", " +
                                     ptf.Telefone + ", " +
                                     ptf.Email + ", " +
                                     ptf.Cep + ", " +
                                     ptf.Logradouro + ", " +
                                     ptf.Bairro + ", " +
                                     ptf.Localidade + ", " +
                                     ptf.Uf + ", " +
                                     ptf.Numero + ", " +
                                     ptf.Complemento + ", " +
                                     ptf.Observacoes;

                    var fieldValues = "'" + p.Nome + "', " +
                                      "'" + p.Cpf + "', " +
                                      "'" + p.Telefone + "', " +
                                      "'" + p.Email + "', " +
                                      "'" + p.Cep + "', " +
                                      "'" + p.Logradouro + "', " +
                                      "'" + p.Bairro + "', " +
                                      "'" + p.Localidade + "', " +
                                      "'" + p.Uf + "', " +
                                      "'" + p.Numero + "', " +
                                      "'" + p.Complemento + "', " +
                                      "'" + p.Observacoes + "'";

                    var query = "INSERT INTO " + p.TableName + " (" + fieldNames + ") VALUES(" + fieldValues + ");";


                    MySqlCommand cmd = new MySqlCommand(query, cc.conn);

                    cmd.ExecuteNonQuery();
                    cc.CloseConnection();
                    MessageBox.Show("Dados inseridos com sucesso.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (cc.IsConnectionOpen() == true)
                    cc.CloseConnection();
                MessageBox.Show("Nenhum dado foi cadastrado... ERRO: " + ex.Message);
            }
            return false;
        }

        public bool Update(PessoaBO p)
        {
            try
            {
                if (cc.OpenConnection() == true && !String.IsNullOrEmpty(p.Id.ToString()))
                {
                    var ptf = new PessoaTableFieldsBO();
                    var updateString = ptf.Nome + "= '" + p.Nome + "', " +
                                     ptf.Cpf + "= '" + p.Cpf + "', " +
                                     ptf.Telefone + "= '" + p.Telefone + "', " +
                                     ptf.Email + "= '" + p.Email + "', " +
                                     ptf.Cep + "= '" + p.Cep + "', " +
                                     ptf.Logradouro + "= '" + p.Logradouro + "', " +
                                     ptf.Bairro + "= '" + p.Bairro + "', " +
                                     ptf.Localidade + "= '" + p.Localidade + "', " +
                                     ptf.Uf + "= '" + p.Uf + "', " +
                                     ptf.Numero + "= '" + p.Numero + "', " +
                                     ptf.Complemento + "= '" + p.Complemento + "', " +
                                     ptf.Observacoes + "= '" + p.Observacoes + "'";

                    var query = "UPDATE " + p.TableName + " SET " + updateString + " WHERE " + ptf.Id + "=" + p.Id + ";";


                    MySqlCommand cmd = new MySqlCommand(query, cc.conn);

                    cmd.ExecuteNonQuery();
                    cc.CloseConnection();
                    MessageBox.Show("Dados atualizados com sucesso.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (cc.IsConnectionOpen() == true)
                    cc.CloseConnection();
                MessageBox.Show("Nenhum dado foi cadastrado... ERRO: " + ex.Message);
            }
            return false;
        }

        public bool Delete(PessoaBO p)
        {
            try
            {
                if (cc.OpenConnection() == true)
                {
                    var ptf = new PessoaTableFieldsBO();
                    var fieldId = ptf.Id;

                    var query = "DELETE FROM " + p.TableName + " where " + fieldId + "='" + p.Id + "';";

                    MySqlCommand cmd = new MySqlCommand(query, cc.conn);

                    cmd.ExecuteNonQuery();
                    cc.CloseConnection();
                    MessageBox.Show("Dados deletados com sucesso.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (cc.IsConnectionOpen() == true)
                    cc.CloseConnection();
                MessageBox.Show("Nenhum dado foi cadastrado... ERRO: " + ex.Message);
            }
            return false;
        }

        public DataSet Select()
        {
            var DS = new DataSet();
            if (cc.OpenConnection() == true)
            {
                var ptf = new PessoaTableFieldsBO();
                var mySQLDA = new MySqlDataAdapter("select * from " + ptf.TablePessoa + ";", cc.conn);
                mySQLDA.Fill(DS);

                cc.CloseConnection();
            }
            return DS;
        }

        public DataSet SelectBySpecificField(string field, string value)
        {
            var DS = new DataSet();
            if (cc.OpenConnection() == true)
            {
                var ptf = new PessoaTableFieldsBO();
                var mySQLDA = new MySqlDataAdapter("select * from " + ptf.TablePessoa + " where " + field + " like '%" + value + "%';", cc.conn);
                mySQLDA.Fill(DS);

                cc.CloseConnection();
            }
            return DS;
        }
    }
}