﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace ProvaSattra.DAO
{
    public class ConnectionClass
    {
        public MySqlConnection conn;
        private string connString = System.Configuration.ConfigurationManager.AppSettings["connectionString"];

        public ConnectionClass()
        {
            Init();
        }
        private void Init()
        {
            conn = new MySqlConnection(connString);
        }
        public bool OpenConnection()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
                return false;

            }
        }
        public bool CloseConnection()
        {
            try
            {
                conn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool IsConnectionOpen()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
                return false;
            else
                return true;
        }
    }
}
