﻿namespace ProvaSattra
{
    partial class SattraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnClearInsert = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtExtraDataInsert = new System.Windows.Forms.TextBox();
            this.txtDistrictInsert = new System.Windows.Forms.TextBox();
            this.txtCityInsert = new System.Windows.Forms.TextBox();
            this.txtNeighborhoodInsert = new System.Windows.Forms.TextBox();
            this.txtComplementInsert = new System.Windows.Forms.TextBox();
            this.txtStreetNumberInsert = new System.Windows.Forms.TextBox();
            this.txtLocationInsert = new System.Windows.Forms.TextBox();
            this.txtCEPInsert = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNumberInsert = new System.Windows.Forms.TextBox();
            this.txtCPFInsert = new System.Windows.Forms.TextBox();
            this.txtEmailInsert = new System.Windows.Forms.TextBox();
            this.txtFullNameInsert = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnInsert = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancelUpdate = new System.Windows.Forms.Button();
            this.btnSearchUpdate = new System.Windows.Forms.Button();
            this.txtIdUpdate = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtExtraDataUpdate = new System.Windows.Forms.TextBox();
            this.txtDistrictUpdate = new System.Windows.Forms.TextBox();
            this.txtCityUpdate = new System.Windows.Forms.TextBox();
            this.txtNeighborhoodUpdate = new System.Windows.Forms.TextBox();
            this.txtComplementUpdate = new System.Windows.Forms.TextBox();
            this.txtStreetNumberUpdate = new System.Windows.Forms.TextBox();
            this.txtLocationUpdate = new System.Windows.Forms.TextBox();
            this.txtCEPUpdate = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtNumberUpdate = new System.Windows.Forms.TextBox();
            this.txtCPFUpdate = new System.Windows.Forms.TextBox();
            this.txtEmailUpdate = new System.Windows.Forms.TextBox();
            this.txtFullNameUpdate = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cmbField = new System.Windows.Forms.ComboBox();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.dgvPessoas = new System.Windows.Forms.DataGridView();
            this.btnExport = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPessoas)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 409);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnClearInsert);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.btnInsert);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 383);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Inserção";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnClearInsert
            // 
            this.btnClearInsert.Location = new System.Drawing.Point(429, 340);
            this.btnClearInsert.Name = "btnClearInsert";
            this.btnClearInsert.Size = new System.Drawing.Size(101, 37);
            this.btnClearInsert.TabIndex = 13;
            this.btnClearInsert.Text = "Limpar Tudo";
            this.btnClearInsert.UseVisualStyleBackColor = true;
            this.btnClearInsert.Click += new System.EventHandler(this.btnClearInsert_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtExtraDataInsert);
            this.groupBox2.Controls.Add(this.txtDistrictInsert);
            this.groupBox2.Controls.Add(this.txtCityInsert);
            this.groupBox2.Controls.Add(this.txtNeighborhoodInsert);
            this.groupBox2.Controls.Add(this.txtComplementInsert);
            this.groupBox2.Controls.Add(this.txtStreetNumberInsert);
            this.groupBox2.Controls.Add(this.txtLocationInsert);
            this.groupBox2.Controls.Add(this.txtCEPInsert);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(9, 172);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(628, 155);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Endereço";
            // 
            // txtExtraDataInsert
            // 
            this.txtExtraDataInsert.Location = new System.Drawing.Point(85, 123);
            this.txtExtraDataInsert.Name = "txtExtraDataInsert";
            this.txtExtraDataInsert.Size = new System.Drawing.Size(280, 20);
            this.txtExtraDataInsert.TabIndex = 11;
            this.txtExtraDataInsert.TextChanged += new System.EventHandler(this.txtExtraDataInsert_TextChanged);
            this.txtExtraDataInsert.LostFocus += new System.EventHandler(this.txtExtraDataInsert_LostFocus);
            // 
            // txtDistrictInsert
            // 
            this.txtDistrictInsert.Location = new System.Drawing.Point(571, 84);
            this.txtDistrictInsert.Name = "txtDistrictInsert";
            this.txtDistrictInsert.ReadOnly = true;
            this.txtDistrictInsert.Size = new System.Drawing.Size(51, 20);
            this.txtDistrictInsert.TabIndex = 10;
            // 
            // txtCityInsert
            // 
            this.txtCityInsert.Location = new System.Drawing.Point(420, 84);
            this.txtCityInsert.Name = "txtCityInsert";
            this.txtCityInsert.ReadOnly = true;
            this.txtCityInsert.Size = new System.Drawing.Size(122, 20);
            this.txtCityInsert.TabIndex = 9;
            // 
            // txtNeighborhoodInsert
            // 
            this.txtNeighborhoodInsert.Location = new System.Drawing.Point(231, 84);
            this.txtNeighborhoodInsert.Name = "txtNeighborhoodInsert";
            this.txtNeighborhoodInsert.ReadOnly = true;
            this.txtNeighborhoodInsert.Size = new System.Drawing.Size(134, 20);
            this.txtNeighborhoodInsert.TabIndex = 8;
            // 
            // txtComplementInsert
            // 
            this.txtComplementInsert.Location = new System.Drawing.Point(86, 84);
            this.txtComplementInsert.Name = "txtComplementInsert";
            this.txtComplementInsert.Size = new System.Drawing.Size(96, 20);
            this.txtComplementInsert.TabIndex = 7;
            this.txtComplementInsert.TextChanged += new System.EventHandler(this.txtComplementInsert_TextChanged);
            this.txtComplementInsert.LostFocus += new System.EventHandler(this.txtComplementInsert_LostFocus);
            // 
            // txtStreetNumberInsert
            // 
            this.txtStreetNumberInsert.Location = new System.Drawing.Point(551, 51);
            this.txtStreetNumberInsert.Name = "txtStreetNumberInsert";
            this.txtStreetNumberInsert.Size = new System.Drawing.Size(71, 20);
            this.txtStreetNumberInsert.TabIndex = 6;
            this.txtStreetNumberInsert.TextChanged += new System.EventHandler(this.txtStreetNumberInsert_TextChanged);
            this.txtStreetNumberInsert.LostFocus += new System.EventHandler(this.txtStreetNumberInsert_LostFocus);
            // 
            // txtLocationInsert
            // 
            this.txtLocationInsert.Location = new System.Drawing.Point(76, 51);
            this.txtLocationInsert.Name = "txtLocationInsert";
            this.txtLocationInsert.ReadOnly = true;
            this.txtLocationInsert.Size = new System.Drawing.Size(416, 20);
            this.txtLocationInsert.TabIndex = 5;
            this.txtLocationInsert.TextChanged += new System.EventHandler(this.txtLocationInsert_TextChanged);
            // 
            // txtCEPInsert
            // 
            this.txtCEPInsert.Location = new System.Drawing.Point(43, 22);
            this.txtCEPInsert.Name = "txtCEPInsert";
            this.txtCEPInsert.Size = new System.Drawing.Size(188, 20);
            this.txtCEPInsert.TabIndex = 4;
            this.txtCEPInsert.TextChanged += new System.EventHandler(this.txtCEPInsert_TextChanged);
            this.txtCEPInsert.LostFocus += new System.EventHandler(this.txtCepInsert_LostFocus);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Observações:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(548, 87);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "UF:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(371, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Cidade:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(188, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Bairro:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Complemento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "CEP:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Logradouro:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(498, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Número:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNumberInsert);
            this.groupBox1.Controls.Add(this.txtCPFInsert);
            this.groupBox1.Controls.Add(this.txtEmailInsert);
            this.groupBox1.Controls.Add(this.txtFullNameInsert);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(9, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(628, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações Básicas";
            // 
            // txtNumberInsert
            // 
            this.txtNumberInsert.Location = new System.Drawing.Point(456, 66);
            this.txtNumberInsert.Name = "txtNumberInsert";
            this.txtNumberInsert.Size = new System.Drawing.Size(166, 20);
            this.txtNumberInsert.TabIndex = 3;
            this.txtNumberInsert.TextChanged += new System.EventHandler(this.txtNumberInsert_TextChanged);
            this.txtNumberInsert.LostFocus += new System.EventHandler(this.txtNumberInsert_LostFocus);
            // 
            // txtCPFInsert
            // 
            this.txtCPFInsert.Location = new System.Drawing.Point(434, 22);
            this.txtCPFInsert.Name = "txtCPFInsert";
            this.txtCPFInsert.Size = new System.Drawing.Size(188, 20);
            this.txtCPFInsert.TabIndex = 1;
            this.txtCPFInsert.TextChanged += new System.EventHandler(this.txtCPFInsert_TextChanged);
            this.txtCPFInsert.LostFocus += new System.EventHandler(this.txtCPFInsert_LostFocus);
            // 
            // txtEmailInsert
            // 
            this.txtEmailInsert.Location = new System.Drawing.Point(54, 66);
            this.txtEmailInsert.Name = "txtEmailInsert";
            this.txtEmailInsert.Size = new System.Drawing.Size(338, 20);
            this.txtEmailInsert.TabIndex = 2;
            this.txtEmailInsert.TextChanged += new System.EventHandler(this.txtEmailInsert_TextChanged);
            this.txtEmailInsert.LostFocus += new System.EventHandler(this.txtEmailInsert_LostFocus);
            // 
            // txtFullNameInsert
            // 
            this.txtFullNameInsert.Location = new System.Drawing.Point(105, 22);
            this.txtFullNameInsert.Name = "txtFullNameInsert";
            this.txtFullNameInsert.Size = new System.Drawing.Size(287, 20);
            this.txtFullNameInsert.TabIndex = 0;
            this.txtFullNameInsert.TextChanged += new System.EventHandler(this.txtFullNameInsert_TextChanged);
            this.txtFullNameInsert.LostFocus += new System.EventHandler(this.txtFullNameInsert_LostFocus);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome Completo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(398, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Telefone:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(398, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "CPF:";
            // 
            // btnInsert
            // 
            this.btnInsert.Enabled = false;
            this.btnInsert.Location = new System.Drawing.Point(536, 340);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(101, 37);
            this.btnInsert.TabIndex = 12;
            this.btnInsert.Text = "Cadastrar";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnDelete);
            this.tabPage3.Controls.Add(this.btnCancelUpdate);
            this.tabPage3.Controls.Add(this.btnSearchUpdate);
            this.tabPage3.Controls.Add(this.txtIdUpdate);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.btnUpdate);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(643, 383);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Alteração/Exclusão";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(432, 343);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(101, 37);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "Deletar Permanentemente";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancelUpdate
            // 
            this.btnCancelUpdate.Location = new System.Drawing.Point(319, 343);
            this.btnCancelUpdate.Name = "btnCancelUpdate";
            this.btnCancelUpdate.Size = new System.Drawing.Size(101, 37);
            this.btnCancelUpdate.TabIndex = 15;
            this.btnCancelUpdate.Text = "Cancelar";
            this.btnCancelUpdate.UseVisualStyleBackColor = true;
            this.btnCancelUpdate.Visible = false;
            this.btnCancelUpdate.Click += new System.EventHandler(this.btnCancelUpdate_Click);
            // 
            // btnSearchUpdate
            // 
            this.btnSearchUpdate.Location = new System.Drawing.Point(111, 6);
            this.btnSearchUpdate.Name = "btnSearchUpdate";
            this.btnSearchUpdate.Size = new System.Drawing.Size(72, 22);
            this.btnSearchUpdate.TabIndex = 1;
            this.btnSearchUpdate.Text = "Procurar...";
            this.btnSearchUpdate.UseVisualStyleBackColor = true;
            this.btnSearchUpdate.Click += new System.EventHandler(this.btnSearchUpdate_Click);
            // 
            // txtIdUpdate
            // 
            this.txtIdUpdate.Location = new System.Drawing.Point(35, 8);
            this.txtIdUpdate.Name = "txtIdUpdate";
            this.txtIdUpdate.Size = new System.Drawing.Size(70, 20);
            this.txtIdUpdate.TabIndex = 0;
            this.txtIdUpdate.TextChanged += new System.EventHandler(this.txtIdUpdate_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "Id:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtExtraDataUpdate);
            this.groupBox3.Controls.Add(this.txtDistrictUpdate);
            this.groupBox3.Controls.Add(this.txtCityUpdate);
            this.groupBox3.Controls.Add(this.txtNeighborhoodUpdate);
            this.groupBox3.Controls.Add(this.txtComplementUpdate);
            this.groupBox3.Controls.Add(this.txtStreetNumberUpdate);
            this.groupBox3.Controls.Add(this.txtLocationUpdate);
            this.groupBox3.Controls.Add(this.txtCEPUpdate);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(6, 155);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(628, 155);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Endereço";
            // 
            // txtExtraDataUpdate
            // 
            this.txtExtraDataUpdate.Location = new System.Drawing.Point(85, 123);
            this.txtExtraDataUpdate.Name = "txtExtraDataUpdate";
            this.txtExtraDataUpdate.ReadOnly = true;
            this.txtExtraDataUpdate.Size = new System.Drawing.Size(280, 20);
            this.txtExtraDataUpdate.TabIndex = 13;
            this.txtExtraDataUpdate.TextChanged += new System.EventHandler(this.txtExtraDataUpdate_TextChanged);
            this.txtExtraDataUpdate.LostFocus += new System.EventHandler(this.txtExtraDataUpdate_LostFocus);
            // 
            // txtDistrictUpdate
            // 
            this.txtDistrictUpdate.Location = new System.Drawing.Point(571, 84);
            this.txtDistrictUpdate.Name = "txtDistrictUpdate";
            this.txtDistrictUpdate.ReadOnly = true;
            this.txtDistrictUpdate.Size = new System.Drawing.Size(51, 20);
            this.txtDistrictUpdate.TabIndex = 12;
            // 
            // txtCityUpdate
            // 
            this.txtCityUpdate.Location = new System.Drawing.Point(420, 84);
            this.txtCityUpdate.Name = "txtCityUpdate";
            this.txtCityUpdate.ReadOnly = true;
            this.txtCityUpdate.Size = new System.Drawing.Size(122, 20);
            this.txtCityUpdate.TabIndex = 11;
            // 
            // txtNeighborhoodUpdate
            // 
            this.txtNeighborhoodUpdate.Location = new System.Drawing.Point(231, 84);
            this.txtNeighborhoodUpdate.Name = "txtNeighborhoodUpdate";
            this.txtNeighborhoodUpdate.ReadOnly = true;
            this.txtNeighborhoodUpdate.Size = new System.Drawing.Size(134, 20);
            this.txtNeighborhoodUpdate.TabIndex = 10;
            // 
            // txtComplementUpdate
            // 
            this.txtComplementUpdate.Location = new System.Drawing.Point(86, 84);
            this.txtComplementUpdate.Name = "txtComplementUpdate";
            this.txtComplementUpdate.ReadOnly = true;
            this.txtComplementUpdate.Size = new System.Drawing.Size(96, 20);
            this.txtComplementUpdate.TabIndex = 9;
            this.txtComplementUpdate.TextChanged += new System.EventHandler(this.txtComplementUpdate_TextChanged);
            this.txtComplementUpdate.LostFocus += new System.EventHandler(this.txtComplementUpdate_LostFocus);
            // 
            // txtStreetNumberUpdate
            // 
            this.txtStreetNumberUpdate.Location = new System.Drawing.Point(551, 51);
            this.txtStreetNumberUpdate.Name = "txtStreetNumberUpdate";
            this.txtStreetNumberUpdate.ReadOnly = true;
            this.txtStreetNumberUpdate.Size = new System.Drawing.Size(71, 20);
            this.txtStreetNumberUpdate.TabIndex = 8;
            this.txtStreetNumberUpdate.TextChanged += new System.EventHandler(this.txtStreetNumberUpdate_TextChanged);
            this.txtStreetNumberUpdate.LostFocus += new System.EventHandler(this.txtStreetNumberUpdate_LostFocus);
            // 
            // txtLocationUpdate
            // 
            this.txtLocationUpdate.Location = new System.Drawing.Point(76, 51);
            this.txtLocationUpdate.Name = "txtLocationUpdate";
            this.txtLocationUpdate.ReadOnly = true;
            this.txtLocationUpdate.Size = new System.Drawing.Size(416, 20);
            this.txtLocationUpdate.TabIndex = 7;
            // 
            // txtCEPUpdate
            // 
            this.txtCEPUpdate.Location = new System.Drawing.Point(43, 22);
            this.txtCEPUpdate.Name = "txtCEPUpdate";
            this.txtCEPUpdate.ReadOnly = true;
            this.txtCEPUpdate.Size = new System.Drawing.Size(188, 20);
            this.txtCEPUpdate.TabIndex = 6;
            this.txtCEPUpdate.TextChanged += new System.EventHandler(this.txtCEPUpdate_TextChanged);
            this.txtCEPUpdate.LostFocus += new System.EventHandler(this.txtCepUpdate_LostFocus);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Observações:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(548, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "UF:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(371, 87);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "Cidade:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(188, 87);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Bairro:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Complemento:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "CEP:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 54);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Logradouro:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(498, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "Número:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtNumberUpdate);
            this.groupBox4.Controls.Add(this.txtCPFUpdate);
            this.groupBox4.Controls.Add(this.txtEmailUpdate);
            this.groupBox4.Controls.Add(this.txtFullNameUpdate);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox4.Location = new System.Drawing.Point(6, 38);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(53);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(628, 100);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Informações Básicas";
            // 
            // txtNumberUpdate
            // 
            this.txtNumberUpdate.Location = new System.Drawing.Point(456, 66);
            this.txtNumberUpdate.Name = "txtNumberUpdate";
            this.txtNumberUpdate.ReadOnly = true;
            this.txtNumberUpdate.Size = new System.Drawing.Size(166, 20);
            this.txtNumberUpdate.TabIndex = 5;
            this.txtNumberUpdate.TextChanged += new System.EventHandler(this.txtNumberUpdate_TextChanged);
            this.txtNumberUpdate.LostFocus += new System.EventHandler(this.txtNumberUpdate_LostFocus);
            // 
            // txtCPFUpdate
            // 
            this.txtCPFUpdate.Location = new System.Drawing.Point(434, 22);
            this.txtCPFUpdate.Name = "txtCPFUpdate";
            this.txtCPFUpdate.ReadOnly = true;
            this.txtCPFUpdate.Size = new System.Drawing.Size(188, 20);
            this.txtCPFUpdate.TabIndex = 3;
            this.txtCPFUpdate.TextChanged += new System.EventHandler(this.txtCPFUpdate_TextChanged);
            this.txtCPFUpdate.LostFocus += new System.EventHandler(this.txtCPFUpdate_LostFocus);
            // 
            // txtEmailUpdate
            // 
            this.txtEmailUpdate.Location = new System.Drawing.Point(54, 66);
            this.txtEmailUpdate.Name = "txtEmailUpdate";
            this.txtEmailUpdate.ReadOnly = true;
            this.txtEmailUpdate.Size = new System.Drawing.Size(338, 20);
            this.txtEmailUpdate.TabIndex = 4;
            this.txtEmailUpdate.TextChanged += new System.EventHandler(this.txtEmailUpdate_TextChanged);
            this.txtEmailUpdate.LostFocus += new System.EventHandler(this.txtEmailUpdate_LostFocus);
            // 
            // txtFullNameUpdate
            // 
            this.txtFullNameUpdate.Location = new System.Drawing.Point(105, 22);
            this.txtFullNameUpdate.Name = "txtFullNameUpdate";
            this.txtFullNameUpdate.ReadOnly = true;
            this.txtFullNameUpdate.Size = new System.Drawing.Size(287, 20);
            this.txtFullNameUpdate.TabIndex = 2;
            this.txtFullNameUpdate.TextChanged += new System.EventHandler(this.txtFullNameUpdate_TextChanged);
            this.txtFullNameUpdate.LostFocus += new System.EventHandler(this.txtFullNameUpdate_LostFocus);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Nome Completo:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(398, 69);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Telefone:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 69);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Email:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(398, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "CPF:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(539, 343);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 37);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "Atualizar";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnExport);
            this.tabPage2.Controls.Add(this.btnRefresh);
            this.tabPage2.Controls.Add(this.cmbField);
            this.tabPage2.Controls.Add(this.txtFilter);
            this.tabPage2.Controls.Add(this.btnFilter);
            this.tabPage2.Controls.Add(this.dgvPessoas);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(643, 383);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consulta";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.btnRefresh.Location = new System.Drawing.Point(469, 8);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Atualizar";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cmbField
            // 
            this.cmbField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbField.FormattingEnabled = true;
            this.cmbField.Location = new System.Drawing.Point(6, 10);
            this.cmbField.Name = "cmbField";
            this.cmbField.Size = new System.Drawing.Size(143, 21);
            this.cmbField.TabIndex = 3;
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(155, 11);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(132, 20);
            this.txtFilter.TabIndex = 2;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // btnFilter
            // 
            this.btnFilter.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.btnFilter.Location = new System.Drawing.Point(295, 8);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Filtrar";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // dgvPessoas
            // 
            this.dgvPessoas.AllowUserToAddRows = false;
            this.dgvPessoas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPessoas.Location = new System.Drawing.Point(-4, 37);
            this.dgvPessoas.Name = "dgvPessoas";
            this.dgvPessoas.ReadOnly = true;
            this.dgvPessoas.Size = new System.Drawing.Size(647, 346);
            this.dgvPessoas.TabIndex = 0;
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.btnExport.Location = new System.Drawing.Point(562, 8);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "Exportar";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 433);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prova Sattra";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPessoas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView dgvPessoas;
        private System.Windows.Forms.ComboBox cmbField;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtExtraDataInsert;
        private System.Windows.Forms.TextBox txtDistrictInsert;
        private System.Windows.Forms.TextBox txtCityInsert;
        private System.Windows.Forms.TextBox txtNeighborhoodInsert;
        private System.Windows.Forms.TextBox txtComplementInsert;
        private System.Windows.Forms.TextBox txtStreetNumberInsert;
        private System.Windows.Forms.TextBox txtLocationInsert;
        private System.Windows.Forms.TextBox txtCEPInsert;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumberInsert;
        private System.Windows.Forms.TextBox txtCPFInsert;
        private System.Windows.Forms.TextBox txtEmailInsert;
        private System.Windows.Forms.TextBox txtFullNameInsert;
        private System.Windows.Forms.TextBox txtIdUpdate;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtExtraDataUpdate;
        private System.Windows.Forms.TextBox txtDistrictUpdate;
        private System.Windows.Forms.TextBox txtCityUpdate;
        private System.Windows.Forms.TextBox txtNeighborhoodUpdate;
        private System.Windows.Forms.TextBox txtComplementUpdate;
        private System.Windows.Forms.TextBox txtStreetNumberUpdate;
        private System.Windows.Forms.TextBox txtLocationUpdate;
        private System.Windows.Forms.TextBox txtCEPUpdate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtNumberUpdate;
        private System.Windows.Forms.TextBox txtCPFUpdate;
        private System.Windows.Forms.TextBox txtEmailUpdate;
        private System.Windows.Forms.TextBox txtFullNameUpdate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearchUpdate;
        private System.Windows.Forms.Button btnClearInsert;
        private System.Windows.Forms.Button btnCancelUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnExport;
    }
}

