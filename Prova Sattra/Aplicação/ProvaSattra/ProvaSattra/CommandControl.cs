﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvaSattra.DAO;
using ProvaSattra.BO;
using System.Data;
using System.Windows.Forms;
using ProvaSattra.ExternalFunctions;

namespace ProvaSattra
{
    public class CommandControl
    {
        private ProvaSattra.DAO.SattraDAO DAO;
        private ProvaSattra.BO.PessoaBO pBO;
        private ProvaSattra.ExternalFunctions.ViaCEP viaCep;

        public CommandControl()
        {
            DAO = new SattraDAO();
            pBO = new PessoaBO();
            viaCep = new ViaCEP();
        }
        public DataSet FillDSTable()
        {
            return DAO.Select();
        }

        public DataSet FillDSTableByFilterKey(string field, string value)
        {
            return DAO.SelectBySpecificField(field, value);
        }

        public PessoaBO GetPessoaBOById(string id)
        {
            var pbo = new PessoaBO();
            try
            {
                var ds = DAO.SelectBySpecificField("id", id);
                pbo = ds.Tables[0].AsEnumerable().Select(dataRow => new PessoaBO {
                    Bairro = dataRow.Field<string>("bairro"),
                    Cep = dataRow.Field<string>("cep"),
                    Complemento = dataRow.Field<string>("complemento"),
                    Cpf = dataRow.Field<string>("cpf"),
                    Email = dataRow.Field<string>("email"),
                    Id = Convert.ToInt16(id),
                    Localidade = dataRow.Field<string>("localidade"),
                    Logradouro = dataRow.Field<string>("logradouro"),
                    Nome = dataRow.Field<string>("nome"),
                    Observacoes = dataRow.Field<string>("observacoes"),
                    Telefone = dataRow.Field<string>("telefone"),
                    Uf = dataRow.Field<string>("uf"),
                    Numero = dataRow.Field<string>("numero")
                }).First();
                return pbo;
            }
            catch 
            {
                MessageBox.Show("Nenhum dado foi carregado pro id " + id);
                return pbo;
            }
        }

        public bool Insert(PessoaBO p)
        {
            return DAO.Insert(p);
        }

        public bool Update(PessoaBO p)
        {
            return DAO.Update(p);
        }

        public bool Delete(PessoaBO p)
        {
            return DAO.Delete(p);
        }

        public void ExportToTxt(DataGridView dgv)
        {
            try
            {
                var separator = "#";
                var dialog = new SaveFileDialog();
                dialog.Filter = "Text File|*.txt";
                var result = dialog.ShowDialog();
                if (result != DialogResult.OK)
                    return;
                var builder = new StringBuilder();
                var rowcount = dgv.Rows.Count;
                var columncount = dgv.Columns.Count;

                for (int i = 0; i < rowcount - 1; i++)
                {
                    separator = "#";
                    var cols = new List<string>();
                    for (int j = 0; j < columncount; j++)
                    {
                        var value = dgv.Rows[i].Cells[j].Value.ToString();

                        if (j == (columncount - 1))
                            separator = "";
                        if (value == "")
                            value = "null";

                        cols.Add(value + separator);
                    }
                    builder.AppendLine(string.Join("\t", cols.ToArray()).Replace("\t",""));
                }
                System.IO.File.WriteAllText(dialog.FileName, builder.ToString());
                MessageBox.Show("Arquivo criado com sucesso");
            }
            catch (Exception ex)
            {
                MessageBox.Show("O arquivo nao pode ser criado" + ex.Message);
            }
            
        }

        public CepBo GetLocationDataByCEP(string cep)
        {
            return viaCep.GetDataByCep(cep);
        }
    }
}
