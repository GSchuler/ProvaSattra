﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProvaSattra.BO;

namespace ProvaSattra
{
    public partial class SattraForm : Form
    {
        private CommandControl cc;
        private ToolTip tt;
        private HashSet<string> dependenciesInsert;
        private HashSet<string> dependenciesUpdate;

        public SattraForm()
        {
            Init();
        }

        private void Init()
        {
            cc = new CommandControl();
            tt = new ToolTip();
            dependenciesInsert = new HashSet<string>();
            dependenciesUpdate = new HashSet<string>();
            InitializeComponent();
            FillCustomFields();
        }

        #region Insert Region

        private void txtFullNameInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtCPFInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtEmailInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtNumberInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtCEPInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
            var cbo = cc.GetLocationDataByCEP(txtCEPInsert.Text);
            txtCityInsert.Text = cbo.Localidade;
            txtDistrictInsert.Text = cbo.Uf;
            txtLocationInsert.Text = cbo.Logradouro;
            txtNeighborhoodInsert.Text = cbo.Bairro;
        }

        private void txtLocationInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtStreetNumberInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtComplementInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }

        private void txtExtraDataInsert_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnInsert;
        }
        private void btnClearInsert_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja apagar as informações deste formulário?", "Prova Sattra", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ClearAll();
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja inserir esses dados?", "Prova Sattra", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var pbo = new PessoaBO()
                {
                    Bairro = txtNeighborhoodInsert.Text,
                    Cep = txtCEPInsert.Text,
                    Complemento = txtComplementInsert.Text,
                    Cpf = txtCPFInsert.Text,
                    Email = txtEmailInsert.Text,
                    Localidade = txtCityInsert.Text,
                    Logradouro = txtLocationInsert.Text,
                    Nome = txtFullNameInsert.Text,
                    Numero = txtStreetNumberInsert.Text,
                    Observacoes = txtExtraDataInsert.Text,
                    Telefone = txtNumberInsert.Text,
                    Uf = txtDistrictInsert.Text
                };

                var insert = cc.Insert(pbo);
                if (insert == true)
                    ClearAll();
            }
        }

        #endregion

        #region Update Region
        private void txtIdUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnSearchUpdate;
        }

        private void txtFullNameUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtCPFUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtEmailUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtNumberUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtCEPUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
            var cbo = cc.GetLocationDataByCEP(txtCEPUpdate.Text);
            txtCityUpdate.Text = cbo.Localidade;
            txtDistrictUpdate.Text = cbo.Uf;
            txtLocationUpdate.Text = cbo.Logradouro;
            txtNeighborhoodUpdate.Text = cbo.Bairro;
        }

        private void txtStreetNumberUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtComplementUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void txtExtraDataUpdate_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnUpdate;
        }

        private void btnSearchUpdate_Click(object sender, EventArgs e)
        {
            var pbo = cc.GetPessoaBOById(txtIdUpdate.Text);

            if (pbo != null)
            {
                FillUpdateSesion(pbo);
                btnUpdate.Visible = true;
                btnCancelUpdate.Visible = true;
                btnDelete.Visible = true;
                DependenciesUpdate();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja alterar as informações desse registro permanentemente?", "Prova Sattra", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var pbo = new PessoaBO()
                {
                    Bairro = txtNeighborhoodUpdate.Text,
                    Cep = txtCEPUpdate.Text,
                    Complemento = txtComplementUpdate.Text,
                    Cpf = txtCPFUpdate.Text,
                    Email = txtEmailUpdate.Text,
                    Id = Convert.ToInt16(txtIdUpdate.Text),
                    Localidade = txtCityUpdate.Text,
                    Logradouro = txtLocationUpdate.Text,
                    Nome = txtFullNameUpdate.Text,
                    Numero = txtStreetNumberUpdate.Text,
                    Observacoes = txtExtraDataUpdate.Text,
                    Telefone = txtNumberUpdate.Text,
                    Uf = txtDistrictUpdate.Text
                };

                var update = cc.Update(pbo);
                if (update == true)
                    ClearAll();
            }
        }

        private void FillUpdateSesion(PessoaBO pbo)
        {
            txtIdUpdate.ReadOnly = true;
            txtFullNameUpdate.Text = pbo.Nome;
            txtFullNameUpdate.ReadOnly = false;
            txtCPFUpdate.Text = pbo.Cpf;
            txtCPFUpdate.ReadOnly = false;
            txtNumberUpdate.Text = pbo.Telefone;
            txtNumberUpdate.ReadOnly = false;
            txtEmailUpdate.Text = pbo.Email;
            txtEmailUpdate.ReadOnly = false;
            txtCEPUpdate.Text = pbo.Cep;
            txtCEPUpdate.ReadOnly = false;
            txtLocationUpdate.Text = pbo.Logradouro;
            txtStreetNumberUpdate.Text = pbo.Numero;
            txtStreetNumberUpdate.ReadOnly = false;
            txtNeighborhoodUpdate.Text = pbo.Bairro;
            txtDistrictUpdate.Text = pbo.Uf;
            txtCityUpdate.Text = pbo.Localidade;
            txtComplementUpdate.Text = pbo.Complemento;
            txtComplementUpdate.ReadOnly = false;
            txtExtraDataUpdate.Text = pbo.Observacoes;
            txtExtraDataUpdate.ReadOnly = false;
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja excluir esse registro permanentemente?", "Prova Sattra", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var pbo = new PessoaBO()
                {
                    Bairro = txtNeighborhoodUpdate.Text,
                    Cep = txtCEPUpdate.Text,
                    Complemento = txtComplementUpdate.Text,
                    Cpf = txtCPFUpdate.Text,
                    Email = txtEmailUpdate.Text,
                    Id = Convert.ToInt16(txtIdUpdate.Text),
                    Localidade = txtCityUpdate.Text,
                    Logradouro = txtLocationUpdate.Text,
                    Nome = txtFullNameUpdate.Text,
                    Numero = txtStreetNumberUpdate.Text,
                    Observacoes = txtExtraDataUpdate.Text,
                    Telefone = txtNumberUpdate.Text,
                    Uf = txtDistrictUpdate.Text
                };

                var delete = cc.Delete(pbo);
                if (delete == true)
                    ClearAll();
            }
        }

        private void btnCancelUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja cancelar a alteração dos dados e limpar o formulário?", "Prova Sattra", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ClearAll();
            }
        }


        #endregion

        #region Consult Region
        private void FillCustomFields()
        {
            //Preenchendo o DataGridView com as informações da tabela Pessoas

            dgvPessoas.DataSource = cc.FillDSTable().Tables[0];

            //Preenchendo o comboBox que vai definir o campo a ser filtrado
            var ptf = new PessoaTableFieldsBO();
            var l = new List<string>()
            {
                ptf.Bairro,
                ptf.Cep,
                ptf.Complemento,
                ptf.Cpf,
                ptf.Email,
                ptf.Id,
                ptf.Localidade,
                ptf.Logradouro,
                ptf.Nome,
                ptf.Observacoes,
                ptf.Telefone
            };
            cmbField.DataSource = l;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            dgvPessoas.DataSource = cc.FillDSTableByFilterKey(cmbField.SelectedValue.ToString(), txtFilter.Text).Tables[0];
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            AcceptButton = btnFilter;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll(); // this one will also fill and update the DataGridView
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            cc.ExportToTxt(dgvPessoas);
        }

        #endregion

        #region ToolTip

        private void PopToolTip(string text, TextBox field)
        {
            tt.Show(text, field, 7000);
            if (text != "Parece tudo ok")
                field.BackColor = Color.Crimson;
            else
                field.BackColor = Color.White;

            DependenciesUpdate(field, text);
        }

        private void txtFullNameInsert_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtFullNameInsert.Text.ValidateNome(), txtFullNameInsert);
        }

        private void txtCPFInsert_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtCPFInsert.Text.ValidateCpf(), txtCPFInsert);
        }

        private void txtEmailInsert_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtEmailInsert.Text.ValidateEmail(), txtEmailInsert);
        }

        private void txtNumberInsert_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtNumberInsert.Text.ValidateTelefone(), txtNumberInsert);
        }

        private void txtCepInsert_LostFocus(object sender, EventArgs e)
        {
            if (txtNeighborhoodInsert.Text == "")
                PopToolTip("CEP Inválido", txtCEPInsert);
            else
                PopToolTip("Parece tudo ok", txtCEPInsert);
        }

        private void txtStreetNumberInsert_LostFocus(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(txtStreetNumberInsert.Text);
                PopToolTip("Parece tudo ok", txtStreetNumberInsert);
            }
            catch
            {
                PopToolTip("Número Inválido", txtStreetNumberInsert);
            }
        }

        private void txtComplementInsert_LostFocus(object sender, EventArgs e)
        {
            if (txtComplementInsert.Text.Length < 255)
                PopToolTip("Parece tudo ok", txtComplementInsert);
            else
                PopToolTip("Complemento muito grande", txtComplementInsert);
        }

        private void txtExtraDataInsert_LostFocus(object sender, EventArgs e)
        {
            if (txtExtraDataInsert.Text.Length < 255)
                PopToolTip("Parece tudo ok", txtExtraDataInsert);
            else
                PopToolTip("Obervações muito grande", txtExtraDataInsert);
        }

        private void txtFullNameUpdate_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtFullNameUpdate.Text.ValidateNome(), txtFullNameUpdate);
        }

        private void txtCPFUpdate_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtCPFUpdate.Text.ValidateCpf(), txtCPFUpdate);
        }

        private void txtEmailUpdate_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtEmailUpdate.Text.ValidateEmail(), txtEmailUpdate);
        }

        private void txtNumberUpdate_LostFocus(object sender, EventArgs e)
        {
            PopToolTip(txtNumberUpdate.Text.ValidateTelefone(), txtNumberUpdate);
        }

        private void txtCepUpdate_LostFocus(object sender, EventArgs e)
        {
            if (txtNeighborhoodUpdate.Text == "")
                PopToolTip("CEP Inválido", txtCEPUpdate);
            else
                PopToolTip("Parece tudo ok", txtCEPUpdate);
        }

        private void txtStreetNumberUpdate_LostFocus(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(txtStreetNumberUpdate.Text);
                PopToolTip("Parece tudo ok", txtStreetNumberUpdate);
            }
            catch
            {
                PopToolTip("Número Inválido", txtStreetNumberUpdate);
            }
        }

        private void txtComplementUpdate_LostFocus(object sender, EventArgs e)
        {
            if (txtComplementUpdate.Text.Length < 255)
                PopToolTip("Parece tudo ok", txtComplementUpdate);
            else
                PopToolTip("Complemento muito grande", txtComplementUpdate);
        }

        private void txtExtraDataUpdate_LostFocus(object sender, EventArgs e)
        {
            if (txtExtraDataUpdate.Text.Length < 255)
                PopToolTip("Parece tudo ok", txtExtraDataUpdate);
            else
                PopToolTip("Obervações muito grande", txtExtraDataUpdate);
        }
        #endregion

        private void ClearAll()
        {
            txtCEPInsert.Clear();
            txtCEPUpdate.Clear();
            txtCityInsert.Clear();
            txtCityUpdate.Clear();
            txtComplementInsert.Clear();
            txtComplementUpdate.Clear();
            txtCPFInsert.Clear();
            txtCPFUpdate.Clear();
            txtDistrictInsert.Clear();
            txtDistrictUpdate.Clear();
            txtEmailInsert.Clear();
            txtEmailUpdate.Clear();
            txtExtraDataInsert.Clear();
            txtExtraDataUpdate.Clear();
            txtFilter.Clear();
            txtFullNameInsert.Clear();
            txtFullNameUpdate.Clear();
            txtIdUpdate.Clear();
            txtLocationInsert.Clear();
            txtLocationUpdate.Clear();
            txtNeighborhoodInsert.Clear();
            txtNeighborhoodUpdate.Clear();
            txtNumberInsert.Clear();
            txtNumberUpdate.Clear();
            txtStreetNumberInsert.Clear();
            txtStreetNumberUpdate.Clear();

            txtCEPInsert.BackColor = Color.White;
            txtCEPUpdate.BackColor = Color.White;
            txtComplementInsert.BackColor = Color.White;
            txtComplementUpdate.BackColor = Color.White;
            txtCPFInsert.BackColor = Color.White;
            txtCPFUpdate.BackColor = Color.White;
            txtEmailInsert.BackColor = Color.White;
            txtEmailUpdate.BackColor = Color.White;
            txtExtraDataInsert.BackColor = Color.White;
            txtExtraDataUpdate.BackColor = Color.White;
            txtFullNameInsert.BackColor = Color.White;
            txtFullNameUpdate.BackColor = Color.White;
            txtNumberInsert.BackColor = Color.White;
            txtNumberUpdate.BackColor = Color.White;
            txtStreetNumberInsert.BackColor = Color.White;
            txtStreetNumberUpdate.BackColor = Color.White;

            txtIdUpdate.ReadOnly = false;
            txtFullNameUpdate.ReadOnly = true;
            txtCPFUpdate.ReadOnly = true;
            txtNumberUpdate.ReadOnly = true;
            txtEmailUpdate.ReadOnly = true;
            txtCEPUpdate.ReadOnly = true;
            txtStreetNumberUpdate.ReadOnly = true;
            txtComplementUpdate.ReadOnly = true;
            txtExtraDataUpdate.ReadOnly = true;

            btnCancelUpdate.Visible = false;
            btnUpdate.Visible = false;
            btnDelete.Visible = false;
            btnDelete.Visible = false;
            btnUpdate.Enabled = false;
            btnInsert.Enabled = false;

            dependenciesInsert.Clear();
            dependenciesUpdate.Clear();


            FillCustomFields();
        }


        private void DependenciesUpdate(TextBox tb = null, string status = null)
        {
            CheckInsertDependencies(tb, status);
            CheckUpdateDependencies(tb, status);
        }

        private void CheckInsertDependencies(TextBox tb = null, string status = null)
        {

            if (txtStreetNumberInsert.Text == "")
                dependenciesInsert.Add(txtStreetNumberInsert.Name);
            if (txtEmailInsert.Text == "")
                dependenciesInsert.Add(txtEmailInsert.Name);
            if (txtCPFInsert.Text == "")
                dependenciesInsert.Add(txtCPFInsert.Name);
            if (txtNumberInsert.Text == "")
                dependenciesInsert.Add(txtNumberInsert.Name);
            if (txtCEPInsert.Text == "")
                dependenciesInsert.Add(txtCEPInsert.Name);
            if (txtStreetNumberInsert.Text == "")
                dependenciesInsert.Add(txtStreetNumberInsert.Name);

            if (tb != null && status != null)
            {
                if (status == "Parece tudo ok")
                {
                    if (dependenciesInsert.Contains(tb.Name))
                        dependenciesInsert.Remove(tb.Name);
                }
                else
                {
                    if (!dependenciesInsert.Contains(tb.Name))
                        dependenciesInsert.Add(tb.Name);
                }
            }
            if (dependenciesInsert.Count == 0)
            {
                btnInsert.Enabled = true;
            }
        }

        private void CheckUpdateDependencies(TextBox tb = null, string status = null)
        {
            if (txtStreetNumberUpdate.Text == "")
                dependenciesUpdate.Add(txtStreetNumberUpdate.Name);
            if (txtEmailUpdate.Text == "")
                dependenciesUpdate.Add(txtEmailUpdate.Name);
            if (txtCPFUpdate.Text == "")
                dependenciesUpdate.Add(txtCPFUpdate.Name);
            if (txtNumberUpdate.Text == "")
                dependenciesUpdate.Add(txtNumberUpdate.Name);
            if (txtCEPUpdate.Text == "")
                dependenciesUpdate.Add(txtCEPUpdate.Name);
            if (txtStreetNumberUpdate.Text == "")
                dependenciesUpdate.Add(txtStreetNumberUpdate.Name);

            if (tb != null && status != null)
            {
                if (status == "Parece tudo ok")
                {
                    if (dependenciesUpdate.Contains(tb.Name))
                        dependenciesUpdate.Remove(tb.Name);
                }
                else
                {
                    if (!dependenciesUpdate.Contains(tb.Name))
                        dependenciesUpdate.Add(tb.Name);
                }
            }
            if (dependenciesUpdate.Count == 0)
            {
                btnUpdate.Enabled = true;
            }
        }


    }
}
